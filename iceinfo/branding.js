var menuItems;
//替换customWelcome和feedback
//window.welcomeUrl = customWelcome? customWelcome:'#';
//window.feedbackUrl = customfeedback? customfeedback:'#';
window.MobileAppName = '在线文档';
window.brandProductName = '在线文档';
localStorage.setItem('UIDefaults_null_CompactMode', 'false');
function ___disableWelcome(onlyOnce) {
	console.info('disableWelcome call, onlyOnce:', onlyOnce);
	var success = false;
	try {
		window.L.Map.Welcome.prototype.shouldWelcome = function(){
			return false;
		}
		success = true;
		console.info('window.L.Map.Welcome.prototype.shouldWelcome mock success');
	} catch (e) {
		// do nothing
	}
	if (!success) {
		if (onlyOnce) {
			console.error('___disableWelcome() fail, not retry on onlyOnce');
		} else {
			console.error('___disableWelcome() fail, retry by setTimeout 100');
			setTimeout(___disableWelcome, 100);
		}
	}
}

function ___disableFeedback(onlyOnce) {
	console.info('disableFeedback call, onlyOnce:', onlyOnce);
	var success = false;
	try {
		window.L.Map.Feedback.prototype.onFeedback = function(){
			// do nothing
		}
		success = true;
		console.info('window.L.Map.Feedback.prototype.onFeedback mock success');
	} catch (e) {
		// do nothing
	}
	if (!success) {
		if (onlyOnce) {
			console.error('___disableFeedback() fail, not retry on onlyOnce');
		} else {
			console.error('___disableFeedback() fail, retry by setTimeout 100');
			setTimeout(___disableFeedback, 100);
		}
	}
}

window.onload = function() {
	___disableWelcome();
	___disableFeedback();
	// wait until the menu (and particularly the document-header) actually exists
	function setMenu() {
		var logoHeader = document.getElementById('document-header');
		if (!logoHeader) {
			// the logo does not exist in the menu yet, re-try in 250ms
			setTimeout(setMenu, 250);
		} else {
			var logo = $('#document-header > div');
			logo.attr('title', '');

			// Handle click on logo
			logo.off('click').on('click', function(e) {
				if (window.parent !== window.self) {
					window.parent.postMessage('click_logo', '*');
				}
			 });
			menuItems = document.querySelectorAll('#main-menu > li > a');


			// toggleuimode
			/* var toggleUImode = document.getElementById('toggleuimode');
			if(toggleUImode){
				toggleUImode.style.display = 'none';
			}
			var menuToggleUImode = document.getElementById('menu-toggleuimode');
			if(menuToggleUImode){
				menuToggleUImode.style.display = 'none';
			} */
		}
	}

	setMenu();
};

document.onkeyup = function(e) {
	if (e.altKey && e.shiftKey) {
		console.log('alt + shift + f');
		menuItems.forEach(function(menuItem) {
			menuItem.style.setProperty('text-decoration', 'underline', 'important');
		});
	}
};
